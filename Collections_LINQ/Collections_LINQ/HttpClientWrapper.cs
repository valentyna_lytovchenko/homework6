﻿using Collections_LINQ.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_LINQ
{
    public class HttpClientWrapper
    {
        private readonly HttpClient HttpClient;

        private static HttpClientWrapper _instance = null;
        public static HttpClientWrapper Instance
        {
            get => _instance != null ? _instance : _instance = new HttpClientWrapper();
            private set => _instance = value;
        }

        private HttpClientWrapper() {
            HttpClient = new HttpClient(); 
        }

        private async Task<string> DataWrapper(string URL)
        {
            string responseBody = "";
            try	
            {
                HttpResponseMessage response = await HttpClient.GetAsync(URL);
                response.EnsureSuccessStatusCode();
                responseBody = await response.Content.ReadAsStringAsync();
            }
            catch(HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");	
                Console.WriteLine("Message :{0} ",e.Message);
            }

            return responseBody;
        }

        public async Task<IEnumerable<TaskJSON>> Select8(int userId) // new
        {
            return JsonConvert.DeserializeObject<IEnumerable<TaskJSON>>(
                await DataWrapper("https://localhost:44316/api/Linq/task8/" + $"{userId}"));
        }

        public async Task<Dictionary<ProjectJSON, int>> Select1(int userId)
        {
            return JsonConvert.DeserializeObject<Dictionary<ProjectJSON, int>>(
                await DataWrapper("https://localhost:44316/api/Linq/task1" + $"{userId}"));
        }

        public async Task<ICollection<TaskJSON>> Select2(int userId)
        {
            return JsonConvert.DeserializeObject<ICollection<TaskJSON>>(
                await DataWrapper("https://localhost:44316/api/Linq/task2" + $"{userId}"));
        }

        public async Task<IEnumerable<(int, string)>> Select3(int userId)
        {
            return JsonConvert.DeserializeObject<IEnumerable<(int, string)>>(
                await DataWrapper("https://localhost:44316/api/Linq/task3" + $"{userId}"));
        }

        public async Task<IEnumerable<(int, string, List<UserJSON>)>> Select4()
        {
            return JsonConvert.DeserializeObject<IEnumerable<(int, string, List<UserJSON>)>>(
                await DataWrapper("https://localhost:44316/api/Linq/task4"));
        }

        public async Task<IEnumerable<(UserJSON, IEnumerable<TaskJSON>)>> Select5()
        {
            return JsonConvert.DeserializeObject<IEnumerable<(UserJSON, IEnumerable<TaskJSON>)>>(
                await DataWrapper("https://localhost:44316/api/Linq/task5"));
        }

        public async Task<(UserJSON, int, TaskJSON, ProjectJSON, int)> Select6(int userId)
        {
            return JsonConvert.DeserializeObject<(UserJSON, int, TaskJSON, ProjectJSON, int)>(
                await DataWrapper("https://localhost:44316/api/Linq/task6" + $"{userId}"));
        }

        public async Task<IEnumerable<(ProjectJSON, TaskJSON, TaskJSON, int)>> Select7()
        {
            return JsonConvert.DeserializeObject<IEnumerable<(ProjectJSON, TaskJSON, TaskJSON, int)>>(
                await DataWrapper("https://localhost:44316/api/Linq/task7"));
        }

        public async Task<List<ProjectJSON>> GetProjects()
        {
            return JsonConvert.DeserializeObject<List<ProjectJSON>>(
                await DataWrapper("https://localhost:44316/api/projects"));               
        }

        public async Task<List<TeamJSON>> GetTeams()
        {
            return JsonConvert.DeserializeObject<List<TeamJSON>>(
                await DataWrapper("https://localhost:44316/api/Teams"));
        }

        public async Task<List<TaskJSON>> GetTasks()
        {
            return JsonConvert.DeserializeObject<List<TaskJSON>>(
                await DataWrapper("https://localhost:44316/api/Tasks"));
        }

        public async Task<List<UserJSON>> GetUsers()
        {
            return JsonConvert.DeserializeObject<List<UserJSON>>(
                await DataWrapper("https://localhost:44316/api/Users"));
        }

    }
}
