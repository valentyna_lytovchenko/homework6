﻿using Collections_LINQ.Models;
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using System.Timers;

namespace Collections_LINQ
{
    public static class Queries
    {
        static HttpClient client = new HttpClient();
        public static TaskCompletionSource<int> completionSource = new TaskCompletionSource<int>();

        public static Task<int> MarkRandomTaskWithDelay(int interval)
        {
            Timer timer = new Timer(interval)
            {
                AutoReset = false
            };

            timer.Elapsed += Marking;

            timer.Start();
            return completionSource.Task;
        }

        private async static void Marking(object o, ElapsedEventArgs e)
        {
            try
            {
                var task = await MarkTaskAsDone();

                Console.WriteLine($"\nTask {task.id} was marked as Done.\n");
                completionSource.SetResult(task.id);
            }
            catch (Exception ex)
            {
                completionSource.SetException(ex);
            }
        }

        private static async Task<TaskJSON> MarkTaskAsDone()
        {
            HttpClientWrapper wrapper = HttpClientWrapper.Instance;
            Random rnd = new Random();

            var allTasks = await wrapper.GetTasks();
            var tasks = allTasks.Where(t => t.state == (int)TaskState.InProgress || t.state == (int)TaskState.ToDo);
            int randomId = rnd.Next(1, tasks.Count());
            var oldTask = tasks.First(t => t.id == randomId);

            TaskJSON newTask = new TaskJSON
            {
                id = oldTask.id,
                name = oldTask.name,
                description = oldTask.description,
                createdAt = oldTask.createdAt,
                performerId = oldTask.performerId,
                projectId = oldTask.projectId,
                state = (int)TaskState.Done,
                finishedAt = DateTime.Now
            };

            await client.PutAsJsonAsync("https://localhost:44316/api/tasks", newTask);
            return newTask;
        }
    }
}
