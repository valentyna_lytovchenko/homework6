﻿using System;
using System.Collections.Generic;

namespace Collections_LINQ.Models
{
    public struct Project
    {
        public int id { get; set; }
        public User author { get; set; }
        public Team team { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public List<Task> tasks { get; set; }
        public DateTime deadline { get; set; }
        public DateTime createdAt { get; set; }

        public override string ToString()
        {
            return $"Project id: {id} {name} ";
        }
    }

}


