﻿using System;

namespace Collections_LINQ.Models
{
    public struct Team
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime createdAt { get; set; }
    }
}
