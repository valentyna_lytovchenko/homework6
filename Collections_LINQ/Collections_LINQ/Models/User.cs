﻿using System;

namespace Collections_LINQ.Models
{
    public struct User
    {
        public int id { get; set; }
        public int? teamId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public DateTime registeredAt { get; set; }
        public DateTime birthDay { get; set; }

        public override string ToString()
        {
            return $"User: id={id}, {firstName}, teamid={teamId}";
        }
    }
}
