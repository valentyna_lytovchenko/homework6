﻿using System;

namespace Collections_LINQ.Models
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
    public struct Task
    {
        public int id { get; set; }
        public int projectId { get; set; }
        public User performer { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int state { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime? finishedAt { get; set; }

        public override string ToString()
        {
            return $"Task id: {id} {name}";
        }

    }
}
