﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Collections_LINQ
{
    class Program
    {
        static async Task MenuShow()
        {
            HttpClientWrapper service = HttpClientWrapper.Instance;

            while (true) // REDO: Menu
            {
                Console.Clear();
                int choice;

                Console.WriteLine("select 1st task");
                Console.WriteLine("select 2nd task");
                Console.WriteLine("select 3rd task");
                Console.WriteLine("select 4th task");
                Console.WriteLine("select 5th task");
                Console.WriteLine("select 6th task");
                Console.WriteLine("select 7th task");
                Console.WriteLine("select 8th(NEW) task");

                Console.Write("-->");
                choice = int.Parse(Console.ReadLine());

                Console.Clear();
                switch (choice)
                {
                    case 1:
                        Console.Write("Enter user id: \n-->");
                        int task1Input = int.Parse(Console.ReadLine());
                        var tsk1 = await service.Select1(task1Input); // task 1
                        Console.Clear();

                        if (tsk1 == null || tsk1.Count <= 0)
                        {
                            Console.WriteLine("No values for this id");                                
                        }
                        foreach (var pair in tsk1)
                        {
                            Console.WriteLine($"Project id: {pair.Key}");
                            Console.WriteLine($"Tasks count: {pair.Value}");
                            Console.WriteLine(new string('-', 50));
                        } 
                        break;
                    case 2:
                        Console.Write("Enter user id: \n-->");
                        int task2Input = int.Parse(Console.ReadLine());
                        var tsk2 = await service.Select2(task2Input); // task 2
                        Console.Clear();
                        if (tsk2 == null || tsk2.Count <= 0)
                        {
                            Console.WriteLine("We have no results for this input");
                            break;
                        }
                        foreach (var item in tsk2)
                        {
                            Console.WriteLine(item);
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    case 3:
                        Console.Write("Enter user id: \n-->");
                        int task3Input = int.Parse(Console.ReadLine());
                        var tsk3 = await service.Select3(task3Input); // task 3
                        Console.Clear();
                        if (tsk3 == null || tsk3.Count() < 0)
                        {
                            Console.WriteLine("We have no results for this input");
                            break;
                        }
                        foreach (var item in tsk3)
                        {
                            Console.WriteLine($"Task id: {item}");
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    case 4:
                        var tsk4 = await service.Select4(); // task 4
                        Console.Clear();
                        foreach (var item in tsk4)
                        {
                            Console.WriteLine(item);
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    case 5:
                        var tsk5 = await service.Select5(); // task 5
                        foreach (var item in tsk5)
                        {
                            Console.WriteLine(item);
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    case 6:
                        Console.Write("Enter user id: \n-->");
                        int task6Input = int.Parse(Console.ReadLine());
                        var tsk6 = await service.Select6(task6Input); // task 6
                        Console.Clear();
                        if (tsk6.ToTuple() == null)
                        {
                            Console.WriteLine("We have no results for this input");
                            break;
                        }
                        Console.WriteLine(tsk6);
                        Console.WriteLine("Not Finished Tasks: " + tsk6);
                        Console.WriteLine("Longest Task: " + tsk6);
                        Console.WriteLine("Count of Last-Project Tasks: " + tsk6);
                        break;
                    case 7:
                        var tsk7 = await service.Select7(); // task 7
                        foreach (var item in tsk7)
                        {
                            Console.WriteLine(item);
                            Console.WriteLine("Longest by description Task: " + item);
                            Console.WriteLine("Shortest by name Task: " + item);
                            Console.WriteLine("Count of performers: " + item);
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    case 8:
                        Console.Write("Enter user id: \n-->");
                        int task8Input = int.Parse(Console.ReadLine());
                        var tsk8 = await service.Select8(task8Input);
                        if (tsk8== null)
                        {
                            Console.WriteLine("We have no results for this input");
                            break;
                        }
                        Console.WriteLine(tsk8.ToString());
                        break;
                    default:
                        return;
                }
                Console.ReadKey();
            }
        }

        static void OldMenuShow()
        {
            while (true)
            {
                Console.Clear();
                int choice;

                Console.WriteLine("select 1st task");
                Console.WriteLine("select 2nd task");
                Console.WriteLine("select 3rd task");
                Console.WriteLine("select 4th task");
                Console.WriteLine("select 5th task");
                Console.WriteLine("select 6th task");
                Console.WriteLine("select 7th task");

                Console.Write("-->");
                choice = int.Parse(Console.ReadLine());

                Console.Clear();
                switch (choice)
                {
                    case 1:
                        Console.Write("Enter user id: \n-->");
                        int task1Input = int.Parse(Console.ReadLine());
                        var tsk1 = LinqService.SelectTasksNumberInProject(task1Input); // task 1
                        Console.Clear();
                        if (tsk1 == null || tsk1.Count < 0)
                        {
                            Console.WriteLine("We have no results for this input");
                            break;
                        }
                        foreach (var item in tsk1)
                        {
                            Console.WriteLine($"Project id: {item.Key.id}");
                            Console.WriteLine($"Tasks count: {item.Value}");
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    case 2:
                        Console.Write("Enter user id: \n-->");
                        int task2Input = int.Parse(Console.ReadLine());
                        var tsk2 = LinqService.SelectTasks(task2Input); // task 2
                        Console.Clear();
                        if (tsk2 == null || tsk2.Count < 0)
                        {
                            Console.WriteLine("We have no results for this input");
                            break;
                        }
                        foreach (var item in tsk2)
                        {
                            Console.WriteLine(item);
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    case 3:
                        Console.Write("Enter user id: \n-->");
                        int task3Input = int.Parse(Console.ReadLine());
                        var tsk3 = LinqService.SelectTasksCompletedInCurrentYear(task3Input); // task 3
                        Console.Clear();
                        if (tsk3 == null || tsk3.Count < 0)
                        {
                            Console.WriteLine("We have no results for this input");
                            break;
                        }
                        foreach (var item in tsk3)
                        {
                            Console.WriteLine($"Task id: {item.id}");
                            Console.WriteLine($"Task name: {item.name}");
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    case 4:
                        var tsk4 = LinqService.SelectTeamsWithMembers(); // task 4
                        Console.Clear();
                        foreach (var item in tsk4)
                        {
                            Console.WriteLine(item.id);
                            Console.WriteLine(item.name);
                            foreach (var user in item.users)
                            {
                                Console.WriteLine(user);
                            }
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    case 5:
                        var tsk5 = LinqService.SelectUsersWithTasks(); // task 5
                        foreach (var item in tsk5)
                        {
                            Console.WriteLine(item.user);
                            foreach (var tsk in item.tasks)
                            {
                                Console.WriteLine("\t" + tsk);
                            }
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    case 6:
                        Console.Write("Enter user id: \n-->");
                        int task6Input = int.Parse(Console.ReadLine());
                        var tsk6 = LinqService.SelectUserTasksInfo(task6Input); // task 6
                        Console.Clear();
                        if (tsk6 == null)
                        {
                            Console.WriteLine("We have no results for this input");
                            break;
                        }
                        Console.WriteLine(tsk6.user);
                        Console.WriteLine("Not Finished Tasks: " + tsk6.countNotFinishedTasks);
                        Console.WriteLine("Longest Task: " + tsk6.longestTask);
                        Console.WriteLine("Count of Last-Project Tasks: " + tsk6.countLastProjectTasks);
                        break;
                    case 7:
                        var tsk7 = LinqService.GetProjectInfo(); // task 7
                        foreach (var item in tsk7)
                        {
                            Console.WriteLine(item.project);
                            Console.WriteLine("Longest by description Task: " + item.longestDescriptionTask);
                            Console.WriteLine("Shortest by name Task: " + item.shortestNameTask);
                            Console.WriteLine("Count of performers: " + item.countUsers);
                            Console.WriteLine(new string('-', 50));
                        }
                        break;
                    default:
                        return;
                }
                Console.ReadKey();
            }           
        }

        static async System.Threading.Tasks.Task Main(string[] args)
        {
            await MenuShow();
            var markedTaskId = await Queries.MarkRandomTaskWithDelay(1000);            
        }
    }
}
