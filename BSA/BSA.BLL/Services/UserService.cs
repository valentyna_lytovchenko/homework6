﻿using System;
using AutoMapper;
using BSA.BLL.Services.Abstract;
using System.Collections.Generic;
using BSA.Common.DTO;
using BSA.DAL.Entities;
using BSA.DAL.Context;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BSA.BLL.Services
{
    public class UserService : BaseService
    {
        public UserService(IMapper mapper, BSADbContext context) : base(mapper, context) { }

        public async Task<IList<UserDTO>> GetUsers()
        {
            var users = await _context.Users.ToListAsync();
            return _mapper.Map<IList<UserDTO>>(users);
        }

        public async Task<UserDTO> GetUserById(int userId)
        {
            if (await CheckEntityExists(userId) == false)
            {
                throw new KeyNotFoundException("User is not registered");
            }

            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);
            return _mapper.Map<UserDTO>(user);
        }

        public async Task<int> CreateUser(UserDTO userDto)
        {
            var newUser = _mapper.Map<User>(userDto);
            var added = _context.Users.Add(newUser);
            await _context.SaveChangesAsync();
            return added.Entity.Id;
        }

        /// <summary>
        /// Deletes a user from db by its id
        /// </summary>
        /// <param name="userId"></param>
        /// <exception cref="KeyNotFoundException"></exception>
        public async System.Threading.Tasks.Task DeleteUser(int userId)
        {
            if (await CheckEntityExists(userId) == false)
            {
                throw new KeyNotFoundException("User is not registered, but user delete is called");
            }

            var user = await _context.Users.FindAsync(userId);
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task UpdateUser(UserDTO userDto)
        {            
            if(await CheckEntityExists(userDto.id) == false)
            {
                throw new KeyNotFoundException("User is not registered, but user update is called");
            }

            var userToUpdate = await _context.Users.FindAsync(userDto.id);
            userToUpdate.BirthDay = userDto.birthDay;
            userToUpdate.Email = userDto.email;
            userToUpdate.FirstName = userDto.firstName;
            userToUpdate.LastName = userDto.lastName;
            if (await _context.Teams.FindAsync(userDto.teamId) == null)
            {
                throw new ArgumentException($"Trying to add user to team with id#{ userDto.teamId }, but no such team ");
            }
            userToUpdate.TeamId = userDto.teamId;

            _context.Users.Update(userToUpdate);
            await _context.SaveChangesAsync();
        }

        protected override async Task<bool> CheckEntityExists(int userId)
        {
            return await _context.Users.FindAsync(userId) != null;
        }
    }
}
