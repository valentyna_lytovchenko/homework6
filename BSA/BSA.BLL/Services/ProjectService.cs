﻿using AutoMapper;
using BSA.BLL.Services.Abstract;
using BSA.Common.DTO;
using BSA.DAL.Context;
using BSA.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA.BLL.Services
{
    public class ProjectService : BaseService
    {
        public ProjectService(IMapper mapper, BSADbContext context) : base(mapper, context) { }

        public async Task<IList<ProjectDTO>> GetProjects()
        {
            var projects = await _context.Projects.ToListAsync();
            return _mapper.Map<IList<ProjectDTO>>(projects);
        }

        public async Task<ProjectDTO> GetProjectById(int projectId)
        {
            var project = await _context.Projects.FindAsync(projectId);
            return _mapper.Map<ProjectDTO>(project);
        }

        public async Task<int> CreateProject(ProjectDTO projectDto)
        {
            var project = _mapper.Map<Project>(projectDto);
            int addedId = _context.Projects.Add(project).Entity.Id;
            await _context.SaveChangesAsync();
            return addedId;
        }

        /// <summary>
        /// Deletes project by its id
        /// </summary>
        /// <param name="projectId"></param>
        /// <exception cref="KeyNotFoundException"></exception>
        public async System.Threading.Tasks.Task DeleteProject(int projectId)
        {
            if (await CheckEntityExists(projectId) == false)
                throw new KeyNotFoundException("Project doesn't exists");
            var project = await _context.Projects.FindAsync(projectId);
            _context.Projects.Remove(project);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task UpdateProject(ProjectDTO projectDto)
        {
            var projectToUpdate = await _context.Projects.FindAsync(projectDto.id);

            projectToUpdate.AuthorId = projectDto.authorId;
            projectToUpdate.Author = await _context.Users.FindAsync(projectDto.authorId);
            projectToUpdate.Deadline = projectDto.deadline;
            projectToUpdate.Description = projectDto.description;
            projectToUpdate.Name = projectDto.name;
            projectToUpdate.TeamId = projectDto.teamId;
            projectToUpdate.Team = await _context.Teams.FindAsync(projectDto.teamId);
            projectToUpdate.Tasks = await _context.Tasks.Where(t => t.ProjectId == projectDto.id).ToListAsync();

            _context.Projects.Update(projectToUpdate);
            await _context.SaveChangesAsync();
        }

        protected override async Task<bool> CheckEntityExists(int projectId)
        {
            return await _context.Projects.FindAsync(projectId) != null;
        }
    }
}
