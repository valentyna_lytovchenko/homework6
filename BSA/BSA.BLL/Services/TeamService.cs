﻿using AutoMapper;
using BSA.BLL.Services.Abstract;
using BSA.Common.DTO;
using BSA.DAL.Context;
using BSA.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA.BLL.Services
{
    public class TeamService : BaseService
    {
        public TeamService(IMapper mapper, BSADbContext context) : base(mapper, context) { }

        public async Task<IList<TeamDTO>> GetTeams()
        {
            var teams = await _context.Teams.ToListAsync();
            return _mapper.Map<IList<TeamDTO>>(teams);
        }

        public async Task<TeamDTO> GetTeamById(int teamId)
        {
            if (await CheckEntityExists(teamId) == false)
                throw new KeyNotFoundException("Team doesn't exist");
            var team = await _context.Teams.FindAsync(teamId);
            return _mapper.Map<TeamDTO>(team);
        }

        public async Task<int> CreateTeam(TeamDTO teamDto)
        {
            var team = _mapper.Map<Team>(teamDto);
            var added = _context.Teams.Add(team);
            await _context.SaveChangesAsync();
            return added.Entity.Id;
        }

        public async System.Threading.Tasks.Task DeleteTeam(int id)
        {
            var team = await _context.Teams.Where(t => t.Id == id).FirstOrDefaultAsync();
            _context.Teams.Remove(team);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task UpdateTeam(TeamDTO teamDto)
        {
            var teamToUpdate = await _context.Teams.FindAsync(teamDto.id);

            teamToUpdate.CreatedAt = teamDto.createdAt;
            teamToUpdate.Name = teamDto.name;

            _context.Teams.Update(teamToUpdate);
            await _context.SaveChangesAsync();
        }

        protected override async Task<bool> CheckEntityExists(int teamId)
        {
            return await _context.Teams.FindAsync(teamId) != null;
        }
    }
}
