﻿using BSA.Common.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA.BLL.Services
{
    public class LinqService
    {
        private ProjectService _projectService;
        private UserService _userService;
        private TaskService _taskService;
        private TeamService _teamService;

        public LinqService(
            ProjectService projectService,
            TeamService teamService,
            TaskService taskService,
            UserService userService) 
        {
            _projectService = projectService;
            _userService = userService;
            _taskService = taskService;
            _teamService = teamService;
        }

        public async Task<Dictionary<ProjectDTO, int>> SelectTasksNumberInProject(int userId) // select №1
        {
            if (!await CheckUserExists (userId))
                throw new KeyNotFoundException("No user with this ID");

            var tasks = await _taskService.GetTasks();
            var projects = await _projectService.GetProjects();

            Dictionary<ProjectDTO, int> selected = (projects.GroupJoin(
                                                    tasks.Where(t => t.performerId == userId),
                                                    proj => proj.id,
                                                    t => t.projectId,
                                                    (proj, tasks) => new KeyValuePair<ProjectDTO, int>(proj, tasks.Count())
                                                 )).Where(x => x.Value > 0).
                                                 ToDictionary(x => x.Key, y => y.Value);
            return selected;
        }

        public async Task<ICollection<TaskDTO>> SelectTasks(int userId) // select №2
        {
            if (!await CheckUserExists(userId))
                throw new KeyNotFoundException("No user with this ID");

            var tasks = await _taskService.GetTasks();
            ICollection<TaskDTO> selected = (from tsk in tasks
                                                 where tsk.performerId == userId
                                                 where tsk.name.Length < 45
                                                 select tsk).ToList();

            return selected;
        }

        public async Task<IEnumerable<(int, string)>> SelectTasksCompletedInCurrentYear(int userId) // select №3
        {
            if (!await CheckUserExists(userId))
                throw new KeyNotFoundException("No user with this ID");

            var tasks = await _taskService.GetTasks();
            var selected = (from tsk in tasks
                            where tsk.performerId == userId
                            where tsk.finishedAt.GetValueOrDefault().Year == DateTime.Now.Year
                            select (
                                tsk.id,
                                tsk.name
                            )).ToList();

            return selected;
        }

        public async Task<IEnumerable<(int, string, List<UserDTO>)>> SelectTeamsWithMembers() // select №4
        {
            var teams = await _teamService.GetTeams();
            var users = await _userService.GetUsers();
            var selected = teams.Select(t => new { t.id, t.name }).GroupJoin(
                    users,
                    t => t.id,
                    u => u.teamId,
                    (anonTeam, usersEnumerable) => (
                        anonTeam.id,
                        anonTeam.name,
                        usersEnumerable.OrderByDescending(u => u.registeredAt).ToList())
                ).ToList();

            return selected;
        }

        public async Task<IEnumerable<(UserDTO, IEnumerable<TaskDTO>)>> SelectUsersWithTasks() // select №5
        {
            var users = await _userService.GetUsers();
            var tasks = await _taskService.GetTasks();
            var selected = users.OrderBy(u => u.firstName).GroupJoin(
                    tasks.OrderByDescending(t => t.name.Length),
                    u => u.id,
                    t => t.performerId,
                    (u, tasks) => ( u, tasks )
                );
            return selected;
        }

        public async Task<(UserDTO, int, TaskDTO, ProjectDTO, int)> SelectUserTasksInfo(int userId) // select №6
        {
            if (!await CheckUserExists(userId))
                throw new KeyNotFoundException("No user with this ID");
            var tasks = await _taskService.GetTasks();
            var users = await _userService.GetUsers();
            var projects = await _projectService.GetProjects();
            var selected = users.Where(u => u.id == userId).GroupJoin(
                    tasks,
                    u => u.id,
                    t => t.performerId,
                    (u, tasks) => new
                    {
                        user = u,
                        countNotFinishedTasks = tasks.Count(t => t.state == TaskState.InProgress 
                                                                || t.state == TaskState.Canceled),
                        longestTask = tasks.OrderBy(t => t.createdAt - t.finishedAt).First()
                    }
                ).GroupJoin(
                    projects,
                    prevStruct => prevStruct.user.teamId,
                    proj => proj.teamId,
                    (prevStruct, projects) =>
                    (
                        prevStruct.user,
                        prevStruct.countNotFinishedTasks,
                        prevStruct.longestTask,
                        projects.OrderBy(p => p.createdAt).Last(),
                        tasks.Where(
                            t => t.projectId == projects.OrderBy(p => p.createdAt).Last().id
                        ).Count()
                    )
            ).FirstOrDefault();
            return selected;
        }

        public async Task<IEnumerable<(ProjectDTO, TaskDTO, TaskDTO, int)>> GetProjectInfo() // select №7
        {
            var tasks = await _taskService.GetTasks();
            var projects = await _projectService.GetProjects();
            var users = await _userService.GetUsers();
            var selected = projects.Select(
                p => (
                    p,
                    tasks.Where(t => t.projectId == p.id).OrderByDescending(t => t.description).First(),
                    tasks.Where(t => t.projectId == p.id).OrderBy(t => t.name).First(),
                    p.description.Length > 20 || tasks.Where(t => t.projectId == p.id).Count() < 3 ?
                    users.Count(u => u.teamId == p.teamId) : -1)
            ).ToList();
            return selected;
        }

        public async Task<IEnumerable<TaskDTO>> SelectUserNotFinishedTasks(int userId) // select №8 (NEW)
        {
            if (!await CheckUserExists(userId))
                throw new KeyNotFoundException("No user with this ID");

            var tasks = await _taskService.GetTasks();
            var selected = tasks.Where(t => t.performerId == userId && t.state != TaskState.Done).ToList();

            return selected;
        }

        private async Task<bool> CheckUserExists(int userId)
        {
            var users = await _userService.GetUsers();
            return users.Where(u => u.id == userId).Count() == 1;
        }
    }
}
