﻿using System;
using AutoMapper;
using System.Linq;
using BSA.Common.DTO;
using BSA.DAL.Context;
using BSA.DAL.Entities;
using System.Threading.Tasks;
using BSA.BLL.Services.Abstract;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace BSA.BLL.Services
{
    public class TaskService : BaseService
    {
        public TaskService(IMapper mapper, BSADbContext context) : base(mapper, context) { }

        public async Task<IList<TaskDTO>> GetTasks()
        {
            var tasks = await _context.Tasks.ToListAsync();
            return _mapper.Map<IList<TaskDTO>>(tasks);
        }

        public async Task<TaskDTO> GetTaskById(int taskId)
        {
            if(await CheckEntityExists(taskId) == false)
            {
                throw new KeyNotFoundException("Task doesn't exist");
            }
            var task = await _context.Tasks.FindAsync(taskId);
            return _mapper.Map<TaskDTO>(task);
        }

        public async Task<int> CreateTask(TaskDTO taskDto)
        {
            var task = _mapper.Map<DAL.Entities.Task>(taskDto);
            task.CreatedAt = DateTime.Now;
            task.Performer = await _context.Users.FindAsync(taskDto.performerId);
            task.PerformerId = task.Performer?.Id;
            var added = _context.Tasks.Add(task);
            await _context.SaveChangesAsync();
            return added.Entity.Id;
        }

        public async System.Threading.Tasks.Task DeleteTask(int taskId)
        {
            if(await CheckEntityExists(taskId) == false)
            {
                throw new KeyNotFoundException("Task doesn't exist");
            }
            var task = await _context.Tasks.Where(t => t.Id == taskId).FirstOrDefaultAsync();
            _context.Tasks.Remove(task);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task UpdateTask(TaskDTO taskDto)
        {
            var taskToUpdate = await _context.Tasks.FindAsync(taskDto.id);
            if (taskToUpdate == null)
            {
                throw new KeyNotFoundException($"Task with id#{taskDto.id} not found");
            }

            taskToUpdate.Title = taskDto.name;
            User performer = null;
            if (taskDto.performerId != null &&
               (performer = await _context.Users.FindAsync(taskDto.performerId)) == null
            )
            {
                throw new ArgumentException("Given performer doesn't exist");
            }
            taskToUpdate.Performer = performer;
            taskToUpdate.PerformerId = taskToUpdate.Performer?.Id;
            taskToUpdate.Description = taskDto.description;
            taskToUpdate.State = (int)taskDto.state;
            taskToUpdate.FinishedAt = taskDto.finishedAt;

            _context.Tasks.Update(taskToUpdate);
            await _context.SaveChangesAsync();
        }
        protected override async Task<bool> CheckEntityExists(int taskId)
        {
            return await _context.Tasks.FindAsync(taskId) != null;
        }
    }
}
