﻿using Xunit;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using BSA.BLL.Services;
using BSA.DAL.Entities;
using BSA.Common.DTO;


namespace BSA.BLL.Tests
{
    [Collection("UnitTests")]
    public class TaskServiceTests : TestBase
    {
        readonly TaskService fakeTaskService;
        public DbSet<Task> FakeTasksDbSet { get; set; }

        public TaskServiceTests()
        {
            fakeTaskService = new TaskService(mapper, fakeContext);
            FakeTasksDbSet = A.Fake<DbSet<Task>>();
            A.CallTo(() => fakeContext.Tasks).Returns(FakeTasksDbSet);
            A.CallTo(() => fakeContext.Tasks.Find(A<int>.Ignored)).Returns(A.Fake<Task>());
            A.CallTo(() => fakeContext.Users).Returns(A.Dummy<DbSet<User>>());
            A.CallTo(() => fakeContext.Users.Find(A<int>.Ignored)).Returns(A.Fake<User>());
        }

        [Fact]
        public async void UpdateTask_WhenSetTaskAsDone_ThenUpdateIsCalled()
        {
            var mockTask = A.Fake<TaskDTO>();
            await fakeTaskService.UpdateTask(mockTask);
            A.CallTo(() => FakeTasksDbSet.Update(A<Task>.Ignored)).MustHaveHappenedOnceExactly();
        }

    }
}
