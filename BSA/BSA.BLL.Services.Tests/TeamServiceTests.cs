﻿using Xunit;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using BSA.BLL.Services;
using BSA.DAL.Entities;
using BSA.Common.DTO;

namespace BSA.BLL.Tests
{
    [Collection("UnitTests")]
    public class TeamServiceTests : TestBase
    {
        readonly TeamService fakeTeamService;
        public DbSet<Team> FakeTeamsDbSet { get; set; }

        public TeamServiceTests()
        {
            fakeTeamService = new TeamService(mapper, fakeContext);
            FakeTeamsDbSet = A.Fake<DbSet<Team>>();
            A.CallTo(() => fakeContext.Teams).Returns(FakeTeamsDbSet);
        }

        [Fact]
        public async void AddTeam_WhenNewTeam_ThenAddIsCalled()
        {
            var mockTeam = A.Fake<TeamDTO>();
            try
            {
            await fakeTeamService.CreateTeam(mockTeam);
            } catch { }
            A.CallTo(() => FakeTeamsDbSet.Add(A<Team>.Ignored)).MustHaveHappenedOnceExactly();
        }

    }
}
