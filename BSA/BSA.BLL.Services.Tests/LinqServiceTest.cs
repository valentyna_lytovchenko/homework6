﻿using BSA.BLL.Services;
using BSA.Common.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BSA.BLL.Tests
{
    [Collection("UnitTests")]
    public class LinqServiceTest : TestBase
    {
        LinqService linqService;
        ProjectService projectService;
        TaskService taskService;
        TeamService teamService;
        UserService userService;
        public LinqServiceTest()
        {
            projectService = new ProjectService(mapper, context);
            taskService = new TaskService(mapper, context);
            teamService = new TeamService(mapper, context);
            userService = new UserService(mapper, context);
            linqService = new LinqService(projectService, teamService, taskService, userService);
        }

        [Fact]
        public async void FirstQuery_Valid()
        {
            var mockProject = new ProjectDTO() {
                id = 1,
                name = "MockProject1",
                description = "Description"
            };
            var mockUser = new UserDTO() {
                id = 1,
                firstName = "Sherlock",
                lastName = "Holmes"
            };
            var mockTask = new TaskDTO() {
                id = 1,
                projectId = 1,
                name = "MockTask1",
                performerId = 1,
                state = TaskState.InProgress
            };
            await projectService.CreateProject(mockProject);
            await userService .CreateUser(mockUser);
            await taskService.CreateTask(mockTask);            

            var resultWhenInserted = await linqService.SelectTasksNumberInProject(userId: 1);
            Assert.Single(resultWhenInserted);
            var queryRecord = resultWhenInserted.First();
            Assert.True(
                queryRecord.Key.name == mockProject.name &&
                queryRecord.Value == 1
            );
            RefreshContext();
        }

        [Fact]
        public async void FirstQuery_InvalidParamsId_ThenError()
        {
            var mockProject = new ProjectDTO()
            {
                id = 1,
                name = "MockProject1",
                teamId = 1,
                description = "Description"
            };
            var mockTask = new TaskDTO()
            {
                id = 1,
                projectId = 1,
                name = "MockTask1",
                performerId = 1,
                state = TaskState.InProgress
            };
            await projectService .CreateProject(mockProject);
            await taskService .CreateTask(mockTask);

            await Assert.ThrowsAsync<KeyNotFoundException>(async () => await linqService.SelectTasksNumberInProject(userId: 1));
            RefreshContext();
        }

        [Fact]
        public async void SecondQuery_Valid()
        {
            var mockUser = new UserDTO()
            {
                id = 1,
                firstName = "Sherlock",
                lastName = "Holmes"
            };
            var mockTask1 = new TaskDTO()
            {
                name = "MockTask1",
                performerId = 1,
                state = TaskState.InProgress
            };
            var mockTask2 = new TaskDTO()
            {
                name = "MockTask2",
                performerId = 1,
                state = TaskState.ToDo
            };
            var mockTask3 = new TaskDTO()
            {
                name = new string('A', 60),
                performerId = 1,
                state = TaskState.ToDo
            };

            await userService.CreateUser(mockUser);
            await taskService.CreateTask(mockTask1);
            await taskService .CreateTask(mockTask2);
            await taskService .CreateTask(mockTask3);

            var resultWhenInserted = await linqService.SelectTasks(userId: 1);
            Assert.NotEmpty((System.Collections.IEnumerable)resultWhenInserted);
            Assert.True(Array.TrueForAll(resultWhenInserted.ToArray(), (TaskDTO tsk) => tsk.name.Length < 45));
            RefreshContext();
        }

        [Theory]
        [InlineData(1)]
        [InlineData(3)]
        [InlineData(4)]
        public async void SecondQuery_ThrowsErrorWhenUserInvalid(int testUserId)
        {
            TaskDTO[] mockTasks = {
                new TaskDTO()
                {
                    name = new string('A', 40),
                    performerId = 1,
                    state = TaskState.ToDo
                },
                new TaskDTO()
                {
                    name = "MockTask2",
                    performerId = 1,
                    state = TaskState.ToDo
                },
                new TaskDTO()
                {
                    name = "MockTask1",
                    performerId = 1,
                    state = TaskState.InProgress
                }
            };
            var mockUser = new UserDTO()
            {
                id = 1,
                firstName = "Sherlock",
                lastName = "Holmes"
            };

            await userService .CreateUser(mockUser);
            Array.ForEach(mockTasks, async t => await taskService.CreateTask(t));

            if (testUserId == mockUser.id)
                Assert.Equal(3, (await linqService.SelectTasks(testUserId)).Count);
            else
                await Assert.ThrowsAsync<KeyNotFoundException>(() => linqService.SelectTasks(testUserId));
            RefreshContext();
        }

        [Fact]
        public async void ThirdQuery_Valid()
        {
            var mockUser = new UserDTO()
            {
                id = 1,
                firstName = "Sherlock",
                lastName = "Holmes"
            };
            TaskDTO[] mockTasks = {
                new TaskDTO()
                {
                    name = new string('A', 40),
                    performerId = 1,
                    state = TaskState.ToDo
                },
                new TaskDTO()
                {
                    name = "MockTask2",
                    performerId = 1,
                    state = TaskState.ToDo
                },
                new TaskDTO()
                {
                    id = 3,
                    name = "MockTask3",
                    performerId = 1,
                    state = TaskState.Done,
                    finishedAt = DateTime.Now
                }
            };

            await userService .CreateUser(mockUser);
            Array.ForEach(mockTasks, async t => await taskService.CreateTask(t));

            var result = await linqService.SelectTasksCompletedInCurrentYear(userId: 1);
            Assert.Single(result);
            Assert.Equal((3, "MockTask3"), result.First());
            RefreshContext();
        }

        [Fact]
        public async void FourthQuery_Valid()
        {
            UserDTO[] mockUsers =
            {
                new UserDTO()
                {
                    id = 1,
                    firstName = "Sherlock",
                    lastName = "Holmes",
                    teamId = 1
                },
                new UserDTO()
                {
                    id = 2,
                    firstName = "Conan",
                    lastName = "Doyle",
                },
                new UserDTO()
                {
                    id = 3,
                    firstName = "Irene",
                    lastName = "Adler",
                    teamId = 1
                }
            };
            TeamDTO mockTeam = new TeamDTO() { name = "London" };

            await teamService.CreateTeam(mockTeam);
            Array.ForEach(mockUsers, async u => await userService.CreateUser(u));

            var result = await linqService.SelectTeamsWithMembers();
            Assert.Single(result);

            // З минулого завдання були tuples,
            // треба було вийти з ситуації без радикальних змін коду, тому Item1, Item2...
            var teamWithUsers = result.First();
            Assert.Equal((1, mockTeam.name), (teamWithUsers.Item1, teamWithUsers.Item2));
            Assert.Equal(2, teamWithUsers.Item3.Count);
            RefreshContext();
        }

        [Fact]
        public async void FifthQuery_Valid()
        {
            UserDTO[] mockUsers =
            {
                new UserDTO()
                {
                    id = 1,
                    firstName = "Sherlock",
                    lastName = "Holmes",
                },
                new UserDTO()
                {
                    id = 2,
                    firstName = "Conan",
                    lastName = "Doyle",
                },
                new UserDTO()
                {
                    id = 3,
                    firstName = "Irene",
                    lastName = "Adler",
                }
            };
            TaskDTO[] mockTasks = {
                new TaskDTO()
                {
                    name = new string('A', 40),
                    performerId = 1,
                    state = TaskState.ToDo
                },
                new TaskDTO()
                {
                    name = "MockTask2",
                    performerId = 3,
                    state = TaskState.ToDo
                },
                new TaskDTO()
                {
                    id = 3,
                    name = "MockTask3",
                    performerId = 1,
                    state = TaskState.Done,
                    finishedAt = DateTime.Now
                }
            };

            Array.ForEach(mockUsers, async u => await userService.CreateUser(u));
            Array.ForEach(mockTasks, async t => await taskService.CreateTask(t));

            var result = await linqService.SelectUsersWithTasks();
            Assert.Equal(3, result.Count());
            Assert.Equal(mockTasks.Count(), result.Select(val => val.Item2.Count()).Sum());
            Assert.Contains("Sherlock", result.Select(val => val.Item1.firstName));
            Assert.Empty(result.Where(val => val.Item1.firstName == "Conan").First().Item2);
            RefreshContext();
        }

    }
}
