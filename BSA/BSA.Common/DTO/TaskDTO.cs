﻿using System;

namespace BSA.Common.DTO
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }

    public class TaskDTO
    {
        public int id { get; set; }
        public int projectId { get; set; }
        public int? performerId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public TaskState state { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime? finishedAt { get; set; }
    }
}
