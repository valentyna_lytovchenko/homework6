﻿using BSA.Common.DTO;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Newtonsoft.Json;
using System.Net;
using BSA.DAL.Context;
using Microsoft.EntityFrameworkCore;

namespace BSA.WebAPI.IntegrationTests
{
    [Collection("IntegrationalTests")]
    public class APITests : IClassFixture<BSAWebAPIApplicationFactory<Startup>>
    {

        HttpClient Client { get; init; }
        BSAWebAPIApplicationFactory<Startup> _fixture;
        BSADbContext _context;
        public APITests(BSAWebAPIApplicationFactory<Startup> fixture)
        {
            _fixture = fixture;
            Client = _fixture.CreateClient();
            _context = new BSADbContext(
              new DbContextOptionsBuilder<BSADbContext>()
              .UseInMemoryDatabase("TestDB").Options
           );
            RefreshContext();
        }

        [Fact]
        public async Task CreateValidProject_ThenCreated()
        {
            var projectDto = new ProjectDTO()
            {
                name = "MockProject",
                description = "Description",
            };
            var response = await Client.PostAsync("api/projects/", ConvertToContentFormat(projectDto));

            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Single(_context.Projects);
            RefreshContext();
        }

        [Fact]
        public async Task CreateInvalidProject_ThenBadRequest()
        {
            var projectDto = new ProjectDTO()
            {
                description = "Description",
            };
            var response = await Client.PostAsync("api/projects/", ConvertToContentFormat(projectDto));

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.Empty(_context.Projects);
            RefreshContext();
        }

        [Fact]
        public async Task DeleteUser_ThenDeleted()
        {
            var mockUser = new UserDTO()
            {
                firstName = "Osip",
                lastName = "Mandelshtam",
                birthDay = DateTime.Parse("12.01.1891"),
                email = "vek.volkodav@gmail.com"
            };
            var responseCreated = await Client.PostAsync("api/users/", ConvertToContentFormat(mockUser));
            var createdUser = await DeserializeObjectFromResponse<UserDTO>(responseCreated.Content);

            Assert.NotNull(createdUser);
            Assert.Single(_context.Users);

            var responseDeleted = await Client.DeleteAsync($"api/users/{createdUser.id}");
            Assert.Equal(HttpStatusCode.NoContent, responseDeleted.StatusCode);
            Assert.Empty(_context.Users);
            RefreshContext();
        }

        [Fact]
        public async Task DeleteUserWhenNoExists_ThenReturnNotFound()
        {
            var responseDeleted = await Client.DeleteAsync($"api/users/10");
            Assert.Equal(HttpStatusCode.NotFound, responseDeleted.StatusCode);
            Assert.Empty(_context.Users);
        }

        [Fact]
        public async Task CreateValidTeam_ThenCreated()
        {
            var mockTeam = new TeamDTO()
            {
                name = "N7"
            };
            var response = await Client.PostAsync("api/teams/", ConvertToContentFormat(mockTeam));

            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Single(_context.Teams);
            Assert.Equal(_context.Teams.First().Name, mockTeam.name);
            var tmp = await DeserializeObjectFromResponse<TeamDTO>(response.Content);
            Assert.Equal(_context.Teams.First().Name, tmp.name);
            RefreshContext();
        }

        [Fact]
        public async Task CreateInvalidTeam_ThenBadRequest()
        {
            var mockTeam = new TeamDTO();
            var response = await Client.PostAsync("api/teams/", ConvertToContentFormat(mockTeam));

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.Empty(_context.Teams);
            RefreshContext();
        }

        [Fact]
        public async Task DeleteValidTask_ThenDeleted()
        {
            var mockProject = new ProjectDTO()
            {
                name = "MockProject",
                description = "Description",
            };
            var mockTask = new TaskDTO()
            {
                name = "Pay my dues",
                state = TaskState.InProgress,
                description = "Immediately",
                projectId = 1
            };
            await Client.PostAsync("api/projects/", ConvertToContentFormat(mockProject));
            var response = await Client.PostAsync("api/tasks/", ConvertToContentFormat(mockTask));

            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Single(_context.Tasks);
            var addedTask = await DeserializeObjectFromResponse<TaskDTO>(response.Content);
            Assert.NotNull(addedTask);

            var deleteResponse = await Client.DeleteAsync($"api/tasks/{addedTask.id}");
            Assert.Equal(HttpStatusCode.NoContent, deleteResponse.StatusCode);
            Assert.Empty(_context.Tasks);

            RefreshContext();
        }

        [Fact]
        public async Task DeleteInvalidTask_ThenNotFound()
        {
            var deleteResponse = await Client.DeleteAsync("api/tasks/1");
            Assert.Equal(HttpStatusCode.NotFound, deleteResponse.StatusCode);
            Assert.Empty(_context.Tasks);

            RefreshContext();
        }

        [Fact]
        public async Task SelectUserTasks_ThenReturnsTasksForGivenUser()
        {
            var mockUser = new UserDTO()
            {
                id = 1,
                firstName = "Sherlock",
                lastName = "Holmes"
            };
            TaskDTO[] mockTasks = {
                new TaskDTO()
                {
                    name = new string('A', 40),
                    state = TaskState.ToDo
                },
                new TaskDTO()
                {
                    name = "MockTask2",
                    state = TaskState.ToDo
                },
                new TaskDTO()
                {
                    name = "MockTask3",
                    state = TaskState.Done,
                    finishedAt = DateTime.Now
                }
            };
            var addedUser = await DeserializeObjectFromResponse<UserDTO>(
                (await Client.PostAsync("/api/users", ConvertToContentFormat(mockUser))).Content
            );
            mockTasks[0].performerId = addedUser.id;
            mockTasks[2].performerId = addedUser.id;
            foreach (var t in mockTasks) {
                await Client.PostAsync("api/tasks", ConvertToContentFormat(t));
            }

            var queryResult = await Client.GetAsync($"/api/linq/task2/{addedUser.id}");
            var resultList = await DeserializeObjectFromResponse<List<TaskDTO>>(queryResult.Content);
            Assert.Equal(HttpStatusCode.OK, queryResult.StatusCode);
            Assert.NotNull(resultList);
            Assert.NotEmpty(resultList);
            Assert.Equal(2, resultList.Count);

            RefreshContext();
        }

        [Fact]
        public async Task SelectUserTasksWhenNoExists_ThenBadRequest()
        {
            var mockUser = new UserDTO()
            {
                id = 1,
                firstName = "Sherlock",
                lastName = "Holmes"
            };

            var addedUser = await DeserializeObjectFromResponse<UserDTO>(
                (await Client.PostAsync("/api/users", ConvertToContentFormat(mockUser))).Content
            );
            var queryResult = await Client.GetAsync($"/api/linq/task2/{addedUser.id+1}");

            Assert.Equal(HttpStatusCode.BadRequest, queryResult.StatusCode);
            RefreshContext();
        }

        [Fact]
        public async Task SelectUserNotFinishedTasks_ThenSelected()
        {
            var mockUser = new UserDTO()
            {
                firstName = "Sherlock",
                lastName = "Holmes"
            };
            TaskDTO[] mockTasks = {
                new TaskDTO()
                {
                    name = new string('A', 40),
                    state = TaskState.ToDo
                },
                new TaskDTO()
                {
                    name = "MockTask2",
                    state = TaskState.InProgress
                },
                new TaskDTO()
                {
                    name = "MockTask3",
                    state = TaskState.Done,
                    finishedAt = DateTime.Now
                }
            };
            var addedUser = await DeserializeObjectFromResponse<UserDTO>(
                (await Client.PostAsync("/api/users", ConvertToContentFormat(mockUser))).Content
            );
            mockTasks[0].performerId = addedUser.id;
            mockTasks[1].performerId = addedUser.id;
            mockTasks[2].performerId = addedUser.id;
            foreach (var t in mockTasks)
            {
                await Client.PostAsync("api/tasks", ConvertToContentFormat(t));
            }

            var queryResult = await Client.GetAsync($"/api/linq/task8/{addedUser.id}");
            var resultList = await DeserializeObjectFromResponse<List<TaskDTO>>(queryResult.Content);
            Assert.Equal(HttpStatusCode.OK, queryResult.StatusCode);
            Assert.NotNull(resultList);
            Assert.NotEmpty(resultList);

            Assert.Equal(2, resultList.Count);
            Assert.True(Array.TrueForAll(resultList.ToArray(), (TaskDTO t) => t.state != TaskState.Done));

            RefreshContext();
        }

        [Fact]
        public async Task SelectUserNotDoneTasksWhenNoExists_ThenBadRequest()
        {
            var mockUser = new UserDTO()
            {
                firstName = "Sherlock",
                lastName = "Holmes"
            };

            var addedUser = await DeserializeObjectFromResponse<UserDTO>(
                (await Client.PostAsync("/api/users", ConvertToContentFormat(mockUser))).Content
            );
            var queryResult = await Client.GetAsync($"/api/linq/task8/{addedUser.id + 1}");

            Assert.Equal(HttpStatusCode.BadRequest, queryResult.StatusCode);
            RefreshContext();
        }


        private StringContent ConvertToContentFormat(object obj) => new(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");

        private async Task<T> DeserializeObjectFromResponse<T>(HttpContent content)
        {
            string value = await content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(value);
        }

        private void RefreshContext()
        {
            _context.Projects.RemoveRange(_context.Projects);
            _context.Tasks.RemoveRange(_context.Tasks);
            _context.Users.RemoveRange(_context.Users);
            _context.Teams.RemoveRange(_context.Teams);
            _context.SaveChanges();
        }
    }
}
