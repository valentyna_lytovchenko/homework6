﻿using BSA.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using BSA.BLL.Services;
using System.Threading.Tasks;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : Controller
    {
        private readonly LinqService _linqService;

        public LinqController(LinqService linqService)
        {
            _linqService = linqService;
        }

        [HttpGet]
        [Route("task1/{userId:int}")]
        public async Task<ActionResult<Dictionary<ProjectDTO, int>>> SelectTasksNumberInProject(int userId)
        {
            try
            {
                return Ok(await _linqService.SelectTasksNumberInProject(userId));
            }
            catch (KeyNotFoundException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("task2/{userId:int}")]
        public async Task<ActionResult<ICollection<TaskDTO>>> SelectTasks(int userId)
        {
            try
            {
                return Ok(await _linqService.SelectTasks(userId));
            }
            catch (KeyNotFoundException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("task3/{userId:int}")]
        public async Task<ActionResult<IEnumerable<(int, string)>>> SelectTasksCompletedInCurrentYear(int userId)
        {
            try
            {
                return Ok(await _linqService.SelectTasksCompletedInCurrentYear(userId));
            }
            catch (KeyNotFoundException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("task4")]
        public async Task<ActionResult<IEnumerable<(int, string, List<UserDTO>)>>> SelectTeamsWithMembers()
        {
            return Ok(await _linqService.SelectTeamsWithMembers());
        }

        [HttpGet]
        [Route("task5")]
        public async Task<ActionResult<IEnumerable<(UserDTO, IEnumerable<TaskDTO>)>>> SelectUsersWithTasks()
        {
            return Ok(await _linqService.SelectUsersWithTasks());
        }

        // GET .../api/linq/selecttask6/3
        [HttpGet]
        [Route("task6/{userId:int}")]
        public async Task<ActionResult<(UserDTO, int, TaskDTO, ProjectDTO, int)>> SelectUserTasksInfo(int userId)
        {
            try
            {
                return Ok(await _linqService.SelectUserTasksInfo(userId));
            }
            catch (KeyNotFoundException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("task7")]
        public async Task<ActionResult<IEnumerable<(ProjectDTO, TaskDTO, TaskDTO, int)>>> GetProjectInfo()
        {
            return Ok(await _linqService.GetProjectInfo());
        }

        [HttpGet]
        [Route("task8/{userId:int}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetNotDoneTasks(int userId)
        {
            try
            {
                return Ok(await _linqService.SelectUserNotFinishedTasks(userId));
            }
            catch (KeyNotFoundException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
