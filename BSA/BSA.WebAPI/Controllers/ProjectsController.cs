﻿using BSA.BLL.Services;
using BSA.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;

        public ProjectsController(ProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<ProjectDTO>>> Get()
        {
            return Ok(await _projectService.GetProjects());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> GetById(int id)
        {
            return Ok(await _projectService.GetProjectById(id));
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] ProjectDTO project)
        {
            if (string.IsNullOrEmpty(project.name))
                return BadRequest("Project must have a name!");

            int addedId = await _projectService.CreateProject(project);
            return new JsonResult(await _projectService.GetProjectById(addedId)) {
                StatusCode = (int)HttpStatusCode.Created
            };
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] ProjectDTO project)
        {
            await _projectService.UpdateProject(project);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _projectService.DeleteProject(id);
            }
            catch(KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
            return NoContent();
        }
    }
}
