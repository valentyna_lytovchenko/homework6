﻿using BSA.BLL.Services;
using BSA.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<UserDTO>>> Get()
        {
            return Ok(await _userService.GetUsers());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetById(int id)
        {
            return Ok(await _userService.GetUserById(id));
        }

        [HttpPost]
        public async Task<JsonResult> Create([FromBody] UserDTO user)
        {
            int addedId = await _userService.CreateUser(user);
            var response = new JsonResult(await _userService.GetUserById(addedId))
            {
                StatusCode = (int)HttpStatusCode.Created
            };
            return response; 
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UserDTO user)
        {
            await _userService.UpdateUser(user);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _userService.DeleteUser(id);
            }
            catch(KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
            return NoContent();
        }
    }
}
