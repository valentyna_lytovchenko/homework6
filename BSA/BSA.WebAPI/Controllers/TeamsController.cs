﻿using BSA.BLL.Services;
using BSA.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamsController(TeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<TeamDTO>>> Get()
        {
            return Ok(await _teamService.GetTeams());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> GetById(int id)
        {
            try
            {
                return Ok(await _teamService.GetTeamById(id));
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] TeamDTO team)
        {
            if(string.IsNullOrEmpty(team.name))
                return BadRequest("Team name is not specified");

            int addedId = await _teamService.CreateTeam(team);
            return new JsonResult(await _teamService.GetTeamById(addedId)) { 
                StatusCode = (int)HttpStatusCode.Created 
            };
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] TeamDTO team)
        {
            await _teamService.UpdateTeam(team);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _teamService.DeleteTeam(id);
                return NoContent();
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
