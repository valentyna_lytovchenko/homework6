﻿using BSA.BLL.Services;
using BSA.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TasksController(TaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<TaskDTO>>> Get()
        {
            return Ok(await _taskService.GetTasks());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> GetById(int id)
        {
            try
            {
                return Ok(await _taskService.GetTaskById(id));
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] TaskDTO task)
        {
            if (string.IsNullOrEmpty(task.name))
                return BadRequest("Task name is not specified");

            var addedId = await _taskService.CreateTask(task);
            return new JsonResult(_taskService.GetTaskById(addedId)) {
                StatusCode = (int)HttpStatusCode.Created
            };
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] TaskDTO task)
        {
            await _taskService.UpdateTask(task);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _taskService.DeleteTask(id);
                return NoContent();
            }
            catch(KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
