using BSA.BLL.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using BSA.BLL.MapingProfiles;
using System.Reflection;
using BSA.DAL.Context;
using Microsoft.EntityFrameworkCore;

namespace BSA.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var migrationAssembly = typeof(BSADbContext).Assembly.GetName().Name;
            services.AddDbContext<BSADbContext>(options =>
                options.UseSqlServer(Configuration["ConnectionStrings:BSADbConnection"], 
                opt => opt.MigrationsAssembly(migrationAssembly)));

            services.AddControllers();

            services.AddScoped<ProjectService>();
            services.AddScoped<TaskService>();
            services.AddScoped<UserService>();
            services.AddScoped<TeamService>();

            services.AddAutoMapper(cfg => {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TeamProfile>();
            }, Assembly.GetExecutingAssembly());

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private static void InitializeDatabase(IApplicationBuilder app) //
        {
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                using var context = scope.ServiceProvider.GetRequiredService<BSADbContext>();
                context.Database.Migrate();
            };
        }
    }
}
