﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BSA.DAL.Entities
{
    public class User : BaseEntity
    {
       public int? TeamId { get; set; }
       public string FirstName { get; set; }
       public string LastName { get; set; }
       public string Email { get; set; }
       public DateTime RegisteredAt { get; private set; }
       public DateTime BirthDay { get; set; }
    }
}

