﻿using BSA.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace BSA.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasOne<Team>()
                .WithMany()
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Project>()
                .HasOne(p => p.Team)
                .WithMany()
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Project>()
                .HasOne(p => p.Author)
                .WithMany()
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Task>()
                .HasOne(p => p.Performer)
                .WithMany()
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Project>()
                .HasMany<Task>()
                .WithOne()
                .HasForeignKey(t => t.ProjectId)
                .OnDelete(DeleteBehavior.Cascade);
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
            new {
                Id = 1,
                TeamId = 1,
                FirstName = "Valia",
                LastName = "Lytovchenko",
                Email = "vallit2001@gmail.com",
                RegisteredAt = DateTime.Now,
                BirthDay = DateTime.Parse("07.02.2001").ToUniversalTime(),
                CreatedAt = DateTime.Now,
                Registered = DateTime.Now
            },
            new {
                Id = 2,
                TeamId = 2,
                FirstName = "Ondrey",
                LastName = "Gorobets",
                Email = "ogo@gmail.com",
                RegisteredAt = DateTime.Now,
                BirthDay = DateTime.Parse("10.04.1998").ToUniversalTime(),
                CreatedAt = DateTime.Now,
                Registered = DateTime.Now
            });

            modelBuilder.Entity<Team>().HasData(
                new { Id = 1, Name = "team1", CreatedAt = DateTime.Now },
                new { Id = 2, Name = "team2", CreatedAt = DateTime.Now }
             );

            modelBuilder.Entity<Task>().HasData(
                new
                {
                    Id = 1,
                    ProjectId = 1,
                    PerformerId = 1,
                    Title = "TASK1",
                    Description = "somedescr1",
                    State = 2,
                    CreatedAt = DateTime.Now,
                    FinishedAt = DateTime.Parse("10.04.2021").ToUniversalTime()
                },
                new
                {
                    Id = 2,
                    ProjectId = 2,
                    PerformerId = 2,
                    Title = "TASK2",
                    Description = "somedescr2",
                    State = 2,
                    CreatedAt = DateTime.Now,
                    FinishedAt = DateTime.Parse("11.06.2021").ToUniversalTime()
                });

            modelBuilder.Entity<Project>().HasData(
                new
                {
                    Id = 1,
                    AuthorId = 1,
                    TeamId = 1,
                    Name = "proj1",
                    Description = "desctproj1",
                    CreatedAt = DateTime.Now,
                    Deadline = DateTime.Parse("01.09.2021").ToUniversalTime()
                },
                new
                {
                    Id = 2,
                    AuthorId = 2,
                    TeamId = 2,
                    Name = "proj2",
                    Description = "desctproj2",
                    CreatedAt = DateTime.Now,
                    Deadline = DateTime.Parse("12.10.2021").ToUniversalTime()
                });
        }
    }
}
